Meteor.subscribe 'projects'
Meteor.subscribe 'tasks'

# Home/Projects
Template.projectsList.helpers
  projects: ->
    Projects.find()

Template.addProject.events
  'keypress input': (event)->
    if event.which is 13  # enter key
      name = event.target.value
      Meteor.call 'addProject', name
      event.target.value = ''


# Project/Tasks
Template.projectRoute.helpers
  project: ->
    Projects.findOne FlowRouter.getParam 'projectId'

Template.projectTitle.helpers
  isEditing: ->
    !!FlowRouter.getQueryParam 'editName'

Template.projectTitle.events
  'click button': ->
    FlowRouter.go 'project', projectId:@_id
  'change input': (event)->
    name = event.target.value
    Meteor.call 'updateProjectName', @_id, name
    

Template.tasksList.helpers
  tasks: ->
    projectId = FlowRouter.getParam 'projectId'
    Tasks.find({project:projectId, completedAt: {$exists: false}}, {sort: updatedAt: -1})
  doneTasks: ->
    projectId = FlowRouter.getParam 'projectId'
    Tasks.find({project:projectId, completedAt: {$exists: true}}, {sort: updatedAt: -1})

Template.taskItem.helpers
  isChecked: ->
    if @completedAt then 'checked' else ''

Template.taskItem.events
  'change input': (event)->
    Meteor.call 'toggleTask', this

Template.addTask.events
  'keypress input': (event)->
    if event.which is 13  # enter key
      name = event.target.value
      Meteor.call 'addTask', name, FlowRouter.getParam 'projectId'
      event.target.value = ''

