FlowRouter.route '/project/:projectId',
  name: 'project'
  action: ->
    BlazeLayout.render 'mainLayout',
       template: 'projectRoute'

FlowRouter.route '/',
  action: ->
    BlazeLayout.render 'mainLayout',
       template: 'homeRoute'
