Meteor.publish 'projects', ->
  Projects.find(owner:@userId)
Meteor.publish 'tasks', ->
  Tasks.find(owner:@userId)

itemObj = {
  createdAt: Date
  updatedAt: Date
  owner: String
  project: String
  completedAt: Match.Optional Date
}

Meteor.methods
  toggleTask: (task)->
    if not Meteor.userId() or task.owner isnt Meteor.userId()
      throw new Meteor.Error 'not-authorized'

    check task, Match.ObjectIncluding itemObj

    if task.completedAt
      # uncomplete
      Tasks.update task, {
        $unset:
          completedAt: ''
        $set:
          updatedAt: new Date()
      }
    else
      # complete
      Tasks.update task, {
        $set:
          completedAt: new Date()
          updatedAt: new Date()
      }


  addTask: (name, projectId)->
    check name, String
    check projectId, String

    if not Meteor.userId()
      throw new Meteor.Error 'not-authorized'

    Tasks.insert
      createdAt: new Date()
      updatedAt: new Date()
      owner: Meteor.userId()

      project: projectId
      name: name

  updateProjectName: (projectId, name)->
    check projectId, String
    check name, String

    if not Meteor.userId()
      throw new Meteor.Error 'not-authorized'

    project = Projects.findOne projectId

    if project.owner isnt @userId
      throw new Meteor.Error 'not-authorized'

    
    Projects.update project, {
      $set:
        name: name
        updatedAt: new Date()
    }


  addProject: (name)->
    check name, String

    if not Meteor.userId()
      throw new Meteor.Error 'not-authorized'

    Projects.insert
      createdAt: new Date()
      updatedAt: new Date()
      owner: Meteor.userId()

      name: name
